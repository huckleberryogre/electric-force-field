# electric-force-field

This is a simple electric force field simulator. 

## Start

```bash
yarn install
yarn run start
```

Just click anywhere on the canvas to add a new particle. Keep clicking to add more particles.
Particle charge is randomly assigned to be either positive or negative. They are draggable too.
Enjoy!

## Test

```bash
yarn run test
```

## Live Demo
https://electric-force-field.vercel.app/