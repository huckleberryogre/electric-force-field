import React from "react";
import {ElectricForceFieldComponent} from "./electricForceField/ElectricForceField";

export const App = () => {
    return (
        <ElectricForceFieldComponent/>
    );
}
