import React from "react";
import ReactDOM from 'react-dom';
import { App } from "./App";
import "normalize.css"
import "./styles.css";

const container = document.getElementById("app") as HTMLElement;
ReactDOM.render(<App />, container);
