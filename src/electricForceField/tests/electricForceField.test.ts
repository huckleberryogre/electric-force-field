import {getDistance, getElectricFieldFromSingleCharge,} from '../utils';

describe('distance', () => {
    test('if distance < line increment, return increment', () => {
        expect(getDistance({x: 1, y: 0}, {x: 1, y: 0})).toEqual(0);
        expect(getDistance({x: 2, y: 10}, {x: 2, y: 10})).toEqual(0);
        expect(getDistance({x: 2, y: 10}, {x: 5, y: 14})).toEqual(5);
        expect(getDistance({x: 2, y: 10}, {x: -1, y: 6})).toEqual(5);
    });

    test('just distance', () => {
        // along OX
        expect(getDistance({x: 2, y: 10}, {x: 20, y: 10})).toEqual(18);
        // along OY
        expect(getDistance({x: 2, y: 10}, {x: 2, y: 20})).toEqual(10);
        // 5-12-13 triangle
        expect(getDistance({x: -5, y: -2}, {x: 0, y: 10})).toEqual(13);
    });
});

describe('electric field', () => {
    test('with single charge at minimal distance', () => {
        expect(getElectricFieldFromSingleCharge({x: 1, y: 0}, {x: 1, y: 0, value: 1})).toEqual(Infinity);
        expect(getElectricFieldFromSingleCharge({x: 2, y: 10}, {x: 2, y: 10, value: 1})).toEqual(Infinity);
        expect(getElectricFieldFromSingleCharge({x: 2, y: 10}, {x: 3, y: 10, value: 1})).toEqual(1);
        expect(getElectricFieldFromSingleCharge({x: 2, y: 10}, {x: 2, y: 11, value: 1})).toEqual(1);
    });

    test('with single charge', () => {
        expect(getElectricFieldFromSingleCharge({x: -5, y: -2}, {x: 0, y: 10, value: 1})).toEqual(1 / Math.pow(13, 2));
    });
});











