/**
 * Axis.
 */
export enum EAxis {
    X = 'x',
    Y = 'y'
}

/**
 * Side.
 */
export enum ESide {
    LEFT = 'left',
    TOP = 'top',
    RIGHT = 'right',
    BOTTOM = 'bottom'
}

/**
 * Charge sign path.
 */
export const ESignPath = {
    POSITIVE: 'M 1,1 V 4 H -1 V 1 H -4 V -1 H -1 V -4 H 1 V -1 H 4 V 1 H 1 Z',
    NEGATIVE: 'M 4 1 H -4 V -1 H 4 Z'
}
