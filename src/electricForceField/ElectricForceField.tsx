import React from 'react';
import {createRef} from 'react';
import round from 'lodash/round';
import isFinite from 'lodash/isFinite';
import {CHARGE_COLOR_NEGATIVE, CHARGE_COLOR_POSITIVE, LINE_ITERATIONS, RAY_COUNT, START_DELTA} from './consts';
import {EAxis, ESide, ESignPath} from './enums';
import {getNextPoint, isPointCloseToCharge} from './utils';
import Draggable, {DraggableEventHandler} from 'react-draggable';
import {NoChargeView} from "./NoChargeView";

/**
 * Interface of point representation.
 *
 * @prop x Ox coordinate.
 * @prop y Oy coordinate.
 */
export interface IPoint {
    x: number,
    y: number
}

/**
 * Charge data interface extends point.
 *
 * @prop value Charge value.
 */
export interface IChargeData extends IPoint {
    value?: number;
}

interface IState {
    charges: IChargeData[];
}

/**
 * Electric force field drawer component.
 */
export class ElectricForceFieldComponent extends React.Component<{}, IState> {

    /** Main canvas aka root svg. */
    rootSVG = createRef<SVGSVGElement>();

    state = {
        charges: []
    };

    /**
     * Calculates current root svg position by side.
     *
     * @param side Side.
     */
    getRootSVGPosition = (side: ESide) => this.rootSVG?.current?.getBoundingClientRect()[side];

    /**
     * Root svg click callback.
     *
     * @param event Event object.
     */
    handleClick = (event: React.MouseEvent<SVGElement>) => {
        if (event.target === this.rootSVG?.current) {
            const {charges} = this.state;
            const innerX = event.clientX - this.getRootSVGPosition(ESide.LEFT)!;
            const innerY = event.clientY - this.getRootSVGPosition(ESide.TOP)!;
            const chargeValue = Math.random() > 0.5 ? 1 : -1;

            this.setState({charges: [...charges, {x: innerX, y: innerY, value: chargeValue}]});
        }
    };

    /**
     * Checks if picture being drawn is still within screen.
     *
     * @param x Ox coordinate.
     * @param y Oy coordinate.
     * @param rootSVG_RIGHT Root svg right coordinate.
     * @param rootSVG_BOTTOM Root svg bottom coordinate.
     */
    isPictureWithinScreen = (x: number, y: number, rootSVG_RIGHT: number, rootSVG_BOTTOM: number) => {
        return x > 0 && y > 0 && x < rootSVG_RIGHT && y < rootSVG_BOTTOM;
    };

    /**
     * Render graphical objects to reference.
     */
    renderDefinitions = () => (
        <defs>
            <radialGradient id="white_spot" cx="0.65" cy="0.7" r="0.75">
                <stop stopColor="#ffffff" offset="0" stopOpacity="0.75"/>
                <stop stopColor="#ffffff" offset="0.1" stopOpacity="0.5"/>
                <stop stopColor="#ffffff" offset="0.6" stopOpacity="0"/>
                <stop stopColor="#000000" offset="0.6" stopOpacity="0"/>
                <stop stopColor="#000000" offset="0.75" stopOpacity="0.05"/>
                <stop stopColor="#000000" offset="0.85" stopOpacity="0.15"/>
                <stop stopColor="#000000" offset="1" stopOpacity="0.55"/>
            </radialGradient>
        </defs>
    );

    renderFieldLines = (charge: IChargeData, chargeIndex: number) => {
        const {charges} = this.state;
        const {x: rootChargeNode_X, y: rootChargeNode_Y} = charge;
        /**
         *  L - line, M - move
         *  https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
         */
        const pathDParameters = Array.from({length: RAY_COUNT}, () => `M${rootChargeNode_X},${rootChargeNode_Y}`);

        for (let rayNumber = 0; rayNumber < RAY_COUNT; rayNumber++) {
            const startPoint_X = rootChargeNode_X + START_DELTA * Math.cos(rayNumber * 2 * Math.PI / RAY_COUNT);
            const startPoint_Y = rootChargeNode_Y + START_DELTA * Math.sin(rayNumber * 2 * Math.PI / RAY_COUNT);
            const lastPoint: IPoint = {x: startPoint_X, y: startPoint_Y};
            const rootSVG_BOTTOM = this.getRootSVGPosition(ESide.BOTTOM);
            const rootSVG_RIGHT = this.getRootSVGPosition(ESide.RIGHT);
            let i = 0;
            let next_X;
            let next_Y;
            let stopDraw;

            do {
                i++;
                const {x, y} = lastPoint;
                const currentPoint = {x, y};
                next_X = round(getNextPoint(charge, currentPoint, charges, EAxis.X), 1);
                next_Y = round(getNextPoint(charge, currentPoint, charges, EAxis.Y), 1);
                lastPoint.x = next_X;
                lastPoint.y = next_Y;
                stopDraw = !this.isPictureWithinScreen(next_X, next_Y, rootSVG_RIGHT!, rootSVG_BOTTOM!) || isPointCloseToCharge(charge, currentPoint, charges);

                // todo: this is workaround for cases where next_X/next_Y may become NaN. Ideally this needs investigation. https://huckleberrydev.atlassian.net/browse/HBOG-31
                // appears when there're charges of an opposite signs
                if (isFinite(next_X) && isFinite(next_Y)) {
                    pathDParameters[rayNumber] += `L${next_X},${next_Y}`;
                } else {
                    break;
                }
            }
            while (i < LINE_ITERATIONS && !stopDraw);
        }

        return (
            <>
                {pathDParameters.map((d, index) => (
                    <path
                        key={`charge-${chargeIndex}-force-line-${index}`}
                        role="force-line"
                        stroke={charge.value! > 0 ? 'red' : 'blue'}
                        fill="none"
                        className='force-line'
                        d={d}
                    />
                ))}
            </>
        )
    };

    handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
        if (event.key === 'Backspace') {
            const {charges} = this.state;

            this.setState({charges: charges.slice(0, -1)});
        }
    }

    handleDrag = (chargeIndex: number): DraggableEventHandler => (_, data) => {
        const {charges} = this.state;
        const newCharges: IChargeData[] = [...charges];
        const {x, y} = data;

        newCharges[chargeIndex] = {
            ...newCharges[chargeIndex],
            x,
            y
        };

        this.setState({charges: newCharges});
    };

    render() {
        const {charges} = this.state;

        return (
            <div tabIndex={0} onKeyDown={this.handleKeyDown}>
                <NoChargeView show={!charges.length} />
                <svg id="electro" className='field' onClick={this.handleClick} ref={this.rootSVG}>
                    {this.renderDefinitions()}
                    {charges.map((charge, index) => (
                        <g key={`charge-force-lines-${index}`}>
                            {this.renderFieldLines(charge, index)}
                        </g>
                    ))}
                    <g key="charge-set">
                        {charges.map((charge: IChargeData, index) => (
                            <Draggable
                                key={`charge-${index}`}
                                onDrag={this.handleDrag(index)}
                                position={{
                                    x: charge.x,
                                    y: charge.y
                                }}
                            >
                                <g>
                                    <g transform={`scale(0.4,-0.4)`}>
                                        <circle
                                            cx="0"
                                            cy="0"
                                            r="46.5"
                                            stroke="none"
                                            fill={charge.value! > 0 ? CHARGE_COLOR_POSITIVE : CHARGE_COLOR_NEGATIVE}
                                        />
                                        <circle
                                            cx="0"
                                            cy="0"
                                            r="46.5"
                                            stroke="#000000"
                                            strokeWidth="1"
                                            fill="url(#white_spot)"
                                        />
                                        <path
                                            role="sign"
                                            d={charge.value! > 0 ? ESignPath.POSITIVE : ESignPath.NEGATIVE}
                                            stroke="none"
                                            fill="#000000"
                                            transform="scale(7,7)"
                                        />
                                    </g>
                                </g>
                            </Draggable>
                        ))}
                    </g>
                </svg>
            </div>

        );
    }
}

