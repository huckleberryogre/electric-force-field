import React from 'react';
import welcome from '../assets/welcome.png';

interface IProps {
    show: boolean;
}
export const NoChargeView = ({show} : IProps) => {
    return show ? (
        <div className='no-charge-view'>
            <img src={welcome} alt="welcome" className='img-welcome'/>
        </div>
    ) : null;
}