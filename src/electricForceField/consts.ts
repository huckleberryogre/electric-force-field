/** Positive charge color. */
export const CHARGE_COLOR_POSITIVE = '#ff0000';

/** Negative charge color. */
export const CHARGE_COLOR_NEGATIVE = '#3355ff';

/** Space between charge point and start of a line. */
export const START_DELTA = 5;

/** Field line increment. */
export const LINE_INCREMENT = 20;

/** Default count of iterations to draw a single force line. */
export const LINE_ITERATIONS = 500;

/** Count of rays exposed. */
export const RAY_COUNT = 14;
